package org.deposits.deposits.entities;


import javax.persistence.Transient;
import java.util.ArrayList;
import java.util.Dictionary;
import java.util.Hashtable;
import java.util.List;

public abstract class Entity {
    @Transient
    protected List<Dictionary> notifications;

    Entity(){
        notifications=new ArrayList();
    }

    public List getNotifications() {
        return notifications;
    }

    public void pushSuccess(String message) {
        Dictionary notification=new Hashtable();
        notification.put("status", "success");
        notification.put("message", message);
        this.notifications.add(notification);
    }

    public void pushError(String message) {
        Dictionary notification=new Hashtable();
        notification.put("status", "error");
        notification.put("message", message);
        this.notifications.add(notification);
    }
}
