package org.deposits.deposits.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Credit extends org.deposits.deposits.entities.Entity {
    
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Id
    protected int id;
    protected double value;
    protected double creditValue;
    protected double installmentValue;
    protected int installments;
    protected int installmentsToPaid;
    protected double percent;
    
    @ManyToOne
    protected Deposit deposit;

    public Credit() {
    }

    public Credit(double value, int installments, double percent, Deposit deposit) {
        this.creditValue =value + value*(percent/100);
        this.installmentValue = (double) Math.round(this.creditValue/installments*100)/100;
        this.installments = installments;
        this.installmentsToPaid = installments;
        this.percent = percent;
        this.value=value;
        this.deposit=deposit;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public double getInstallmentValue() {
        return installmentValue;
    }

    public void setInstallmentValue(double installmentValue) {
        this.installmentValue = installmentValue;
    }

    public int getInstallments() {
        return installments;
    }

    public void setInstallments(int installments) {
        this.installments = installments;
    }

    public int getInstallmentsToPaid() {
        return installmentsToPaid;
    }

    public void setInstallmentsToPaid(int installmentsToPaid) {
        this.installmentsToPaid = installmentsToPaid;
    }

    public Deposit getDeposit() {
        return deposit;
    }

    public void setDeposit(Deposit deposit) {
        this.deposit = deposit;
    }
}
