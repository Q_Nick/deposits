package org.deposits.deposits.entities;

import javax.persistence.*;
import javax.persistence.Entity;

@Entity
public class Worker extends org.deposits.deposits.entities.Entity {

    @GeneratedValue(strategy=GenerationType.AUTO)
    @Id
    protected int id;

    @Column(unique=true)
    protected String username;
    protected String password;

    public Worker() {
    }

    public Worker(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
