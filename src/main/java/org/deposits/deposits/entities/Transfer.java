package org.deposits.deposits.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import java.util.Date;

@Entity
public class Transfer extends org.deposits.deposits.entities.Entity {
    
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Id
    protected int id;
    protected double value;
    protected Date date;
    
    @ManyToOne
    protected Deposit deposit;

    public Transfer() {
    }

    public Transfer(double value) {
        this.value = value;
        this.date=new Date();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public Deposit getDeposit() {
        return deposit;
    }

    public void setDeposit(Deposit deposit) {
        this.deposit = deposit;
    }
    
    
    
    
}
