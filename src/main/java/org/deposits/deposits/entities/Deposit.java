package org.deposits.deposits.entities;

import javax.persistence.*;
import javax.persistence.Entity;

@Entity
public class Deposit extends org.deposits.deposits.entities.Entity {

    @GeneratedValue(strategy=GenerationType.AUTO)
    @Id
    protected int id;
    protected double value=0;
    
    protected String depositNumber;
    
    @ManyToOne
    protected Person person;

    public Deposit() {
    }

    public Deposit(String depositNumber) {
        this.depositNumber = depositNumber;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDepositNumber() {
        return depositNumber;
    }

    public void setDepositNumber(String depositNumber) {
        this.depositNumber = depositNumber;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }
}
