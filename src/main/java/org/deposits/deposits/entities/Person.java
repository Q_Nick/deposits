package org.deposits.deposits.entities;

import javax.persistence.*;
import javax.persistence.Entity;

@Entity
public class Person extends org.deposits.deposits.entities.Entity {


    @GeneratedValue(strategy=GenerationType.AUTO)
    @Id
    protected int id;
    protected String firstName;
    protected String lastName;
    protected String pesel;
    
    @ManyToOne
    protected Address address;

    public Person(String firstName, String lastName, String pesel) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.pesel = pesel;
    }

    public Person() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPesel() {
        return pesel;
    }

    public void setPesel(String pesel) {
        this.pesel = pesel;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }
}
