package org.deposits.deposits.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Address extends org.deposits.deposits.entities.Entity {
    
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Id
    protected int id;
    
    protected String city;
    protected String street;
    protected String addressCode;
    protected String buildingNumber;
    protected String flatNumber;

    public Address(String city, String street, String addressCode, String buildingNumber, String flatNumber) {
        this.city = city;
        this.street = street;
        this.addressCode = addressCode;
        this.buildingNumber = buildingNumber;
        this.flatNumber = flatNumber;
    }

    public Address() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getAddressCode() {
        return addressCode;
    }

    public void setAddressCode(String addressCode) {
        this.addressCode = addressCode;
    }

    public String getBuildingNumber() {
        return buildingNumber;
    }

    public void setBuildingNumber(String buildingNumber) {
        this.buildingNumber = buildingNumber;
    }

    public String getFlatNumber() {
        return flatNumber;
    }

    public void setFlatNumber(String flatNumber) {
        this.flatNumber = flatNumber;
    }
    
    
}
