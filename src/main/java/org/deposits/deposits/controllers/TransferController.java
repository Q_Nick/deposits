package org.deposits.deposits.controllers;

import java.util.List;
import org.deposits.deposits.entities.Deposit;
import org.deposits.deposits.entities.Transfer;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@CrossOrigin("*")
@RestController
@RequestMapping("/api/transfers")
public class TransferController extends Controller{

    @GetMapping
    List<Transfer> index(){
        return ts.get();
    }

    @GetMapping("/from/{depositId}")
    List<Transfer> getByDeposit(@PathVariable int depositId){
        return ts.get(ds.get(depositId));
    }
    
    @PostMapping("/{depositId}")
    Transfer create(@PathVariable int depositId, @RequestBody Transfer transfer){
        System.out.println("----------------------->"+transfer.getValue());
        return ts.create(transfer, ds.get(depositId));
    }

    @GetMapping("/{id}")
    Transfer read(@PathVariable int id){
        return ts.get(id);
    }

    @PutMapping("/{id}")
    Transfer update(@PathVariable int id, @RequestBody Transfer newTransfer){
        ts.updateValue(id, newTransfer.getValue());
        ts.updateDeposit(id, newTransfer.getDeposit());

        return ts.get(id);
    }

    @DeleteMapping("/{id}")
    boolean delete(@PathVariable int id){
        return ts.remove(id);
    }
}
