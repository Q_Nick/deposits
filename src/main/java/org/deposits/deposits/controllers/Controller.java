package org.deposits.deposits.controllers;

import org.deposits.deposits.services.*;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class Controller {

    @Autowired
    public AddressService as;
    @Autowired
    public DepositService ds;
    @Autowired
    public PersonService ps;
    @Autowired
    public TransferService ts;
    @Autowired
    public CreditService cs;
    @Autowired
    public WorkerService ws;


}
