package org.deposits.deposits.controllers;

import org.deposits.deposits.entities.Worker;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/workers")
public class WorkerController extends Controller {

    @PostMapping()
    public Worker store(@RequestBody Worker worker){
        return ws.store(worker);
    }


}
