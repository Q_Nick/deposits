package org.deposits.deposits.controllers;

import net.minidev.json.JSONObject;
import org.deposits.deposits.entities.Credit;
import org.deposits.deposits.entities.Deposit;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.text.ParseException;
import java.util.List;

@CrossOrigin("*")
@RestController
@RequestMapping("/api/credits")
public class CreditController extends Controller {


    @GetMapping
    public List<Credit> index() {
        return cs.get();
    }

    @GetMapping("/from/{depositId}")
    public List<Credit> getCreditsByDeposit(@PathVariable int depositId) {
        return cs.get(ds.get(depositId));
    }

    @GetMapping("/{creditId}")
    public Credit get(@PathVariable int creditId) {
        return cs.get(creditId);
    }

    @PostMapping("/{depositId}")
    public Credit create(@RequestBody JSONObject json, @PathVariable int depositId) {
        Deposit deposit = ds.get(depositId);
        Credit credit = new Credit();
        boolean correct = true;
        double value=0;
        int installments=0;
        double percent=0;

        if (json.get("value") != null && json.get("installments") != null && json.get("percent") != null) {
            try {
                value = Double.parseDouble(json.get("value").toString());
            } catch (NumberFormatException e) {
                credit.pushError("Niepoprawna wartość kredytu");
                correct = false;
            }

            try {
                installments = Integer.parseInt(json.get("installments").toString());
            } catch (NumberFormatException e) {
                credit.pushError("Niepoprawna ilość rat kredytu");
                correct = false;
            }

            try {
                percent = Double.parseDouble(json.get("percent").toString());
            } catch (NumberFormatException e) {
                credit.pushError("Niepoprawne oprocentowanie kredytu");
                correct = false;
            }
        }else{
            credit.pushError("Brak wymaganych danych");
            correct = false;
        }

        if(deposit==null){
            credit.pushError("Wybrana lokata nie istnieje");
            correct = false;
        }

        if(correct){
            return cs.create(new Credit(value, installments, percent, deposit));
        }

        return credit;
    }

    @PutMapping("/payInstallment/{creditId}")
    public boolean update(@PathVariable int creditId) {
        if (cs.paidInstallmentFromDeposit(get(creditId))) {
            return true;
        } else {
            return false;
        }
    }
}
