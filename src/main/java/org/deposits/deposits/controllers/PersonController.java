package org.deposits.deposits.controllers;

import org.deposits.deposits.controllers.Controller;
import java.util.List;
import java.util.regex.Pattern;

import org.deposits.deposits.entities.Address;
import org.deposits.deposits.entities.Deposit;
import org.deposits.deposits.entities.Person;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@CrossOrigin("*")
@RestController
@RequestMapping("/api/persons")
public class PersonController extends Controller {

    @GetMapping
    public List<Person> index() {
        return ps.get();
    }
    
    @PostMapping("/{addressId}")
    Person create(@RequestBody Person person, @PathVariable String addressId) {
        Person result=new Person();
        boolean correct=true;

        try{
            Address address=as.get(Integer.parseInt(addressId));
            if(address==null){
                result.pushError("Nie ma takiego adresu");
            }
            person.setAddress(address);
        }catch(Exception e){
            result.pushError("Nie ma takiego adresu");
            correct=false;
        }

        if(!Pattern.matches("^[a-zA-ZąćęłńóśźżĄĘŁŃÓŚŹŻ]+\\x20?\\-?[a-zA-ZąćęłńóśźżĄĘŁŃÓŚŹŻ]+$", person.getFirstName())){
            result.pushError("Niepoprawne imie");
            correct=false;
        }

        if(!Pattern.matches("^[a-zA-ZąćęłńóśźżĄĘŁŃÓŚŹŻ]+\\x20?\\-?[a-zA-ZąćęłńóśźżĄĘŁŃÓŚŹŻ]+$", person.getLastName())){
            result.pushError("Niepoprawne nazwisko");
            correct=false;
        }

        if(!Pattern.matches("^[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]$", person.getPesel())){
            result.pushError("Niepoprawny numer pesel");
            correct=false;
        }

        if(correct){
            return ps.create(person);
        }

        return result;
    }
    
    @GetMapping("/{id}")
    Person read(@PathVariable int id){
        return ps.get(id);
    }

    @PutMapping("/{id}")
    Person update(@RequestBody Person newPerson, @PathVariable int id) {
        ps.updateAddress(id, newPerson.getAddress());
        ps.updateFirstName(id, newPerson.getFirstName());
        ps.updateLastName(id, newPerson.getLastName());
        ps.updatePesel(id, newPerson.getPesel());

        return ps.get(id);
    }
    
    @DeleteMapping("/{id}")
    boolean delete(@PathVariable int id){
        return ps.remove(id);
    }
}
