package org.deposits.deposits.controllers;

import org.deposits.deposits.entities.Address;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import java.util.regex.Pattern;

@CrossOrigin("*")
@RestController
@RequestMapping("/api/address")
public class AddressController extends Controller {



    @GetMapping
    List<Address> index(){
        return as.get();
    }

    @PostMapping
    Address create(@RequestBody Address newAddress){
        Address result=new Address();
        Boolean correct=true;

        if(!Pattern.matches("^[a-zA-ZąćęłńóśźżĄĘŁŃÓŚŹŻ]+\\x20?\\-?[a-zA-ZąćęłńóśźżĄĘŁŃÓŚŹŻ]+$", newAddress.getCity())){
            result.pushError("Niepoprawna nazwa miasta");
            correct=false;
        }

        if(!Pattern.matches("^[1-9][1-9]\\-[1-9][1-9][1-9]$", newAddress.getAddressCode())){
            result.pushError("Niepoprawny kod adresowy");
            correct=false;
        }

        if(!Pattern.matches("^[1-9]+[a-zA-Z]?$", newAddress.getBuildingNumber())){
            result.pushError("Niepoprawny numer budynku");
            correct=false;
        }

        if(!Pattern.matches("^[1-9]+[a-zA-Z]?$", newAddress.getFlatNumber()) && newAddress.getFlatNumber()!=null){
            result.pushError("Niepoprawny numer mieszkania");
            correct=false;
        }

        if(!Pattern.matches("^[a-zA-ZąćęłńóśźżĄĘŁŃÓŚŹŻ]+\\x20?\\-?[a-zA-ZąćęłńóśźżĄĘŁŃÓŚŹŻ]+$", newAddress.getStreet()) && newAddress.getStreet()!=null){
            result.pushError("Niepoprawna nazwa ulicy");
            correct=false;
        }

        if(correct){
            return as.create(newAddress);
        }

        return result;

    }

    @GetMapping("/{id}")
    Address read(@PathVariable int id){
        return as.get(id);
    }

    @PutMapping("/{id}")
    Address update(@PathVariable int id, @RequestBody Address newAddress){

        as.updateCity(id, newAddress.getCity());
        as.updateAddressCode(id, newAddress.getAddressCode());
        as.updateStreet(id, newAddress.getStreet());
        as.updateBuildingNumber(id, newAddress.getBuildingNumber());
        as.updateFlatNumber(id, newAddress.getFlatNumber());

        return as.get(id);
    }

    @DeleteMapping("/{id}")
    boolean delete(@PathVariable int id){
        return as.remove(id);
    }
}
