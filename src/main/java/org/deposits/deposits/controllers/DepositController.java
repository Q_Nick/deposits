package org.deposits.deposits.controllers;

import java.util.List;
import java.util.regex.Pattern;

import org.deposits.deposits.entities.Deposit;
import org.springframework.web.bind.annotation.*;
@CrossOrigin("*")
@RestController
@RequestMapping("/api/deposits")
public class DepositController extends Controller {



    @PostMapping("/{personId}")
    Deposit create(@RequestBody Deposit newDeposit, @PathVariable int personId){
        boolean correct=true;
        Deposit result= new Deposit();

        if(!Pattern.matches("^[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]$", newDeposit.getDepositNumber())){
            correct=false;
            result.pushError("Niepoprawny numer lokaty (wymagane 26 cyfr)");
        }

        if(ps.get(personId)==null){
            correct=false;
            result.pushError("Wybrana osoba nie istnieje");
        }

        if(correct){
            return ds.create(newDeposit, ps.get(personId));
        }

        return result;
    }
    
    @GetMapping
    List<Deposit> index(){
        return ds.get();
    }
    
    @GetMapping("/{id}")
    Deposit read(@PathVariable int id){
        return ds.get(id);
    }

    @GetMapping("/person/{personId}")
    List<Deposit> getDeposits(@PathVariable String personId){
        try {
            return ds.get(ps.get(Integer.parseInt(personId)));
        }catch(Exception e){
            return null;
        }
    }
    
    @PutMapping("/{id}")
    Deposit update(@RequestBody Deposit newDeposit, @PathVariable int id) {
        ds.updateDepositNumber(id, newDeposit.getDepositNumber());
        ds.updatePerson(id, newDeposit.getPerson());

        return ds.get(id);
    }
    
    @DeleteMapping("/{id}")
    boolean delete(@PathVariable int id){
        return ds.remove(id);
    }
    
}
