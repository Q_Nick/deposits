package org.deposits.deposits.controllers;

import org.deposits.deposits.entities.Worker;
import org.springframework.web.bind.annotation.*;

@CrossOrigin
@RestController
@RequestMapping("api/")
public class SessionController extends Controller {

    @PostMapping("login")
    public Object login(@RequestBody Worker worker){
        Worker validatedWorker= ws.get(worker.getUsername(), worker.getPassword());

        if(validatedWorker!=null){
            return validatedWorker;
        }
        else return false;
    }
}
