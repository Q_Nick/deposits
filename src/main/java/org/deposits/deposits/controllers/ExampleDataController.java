package org.deposits.deposits.controllers;

import org.deposits.deposits.entities.*;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin("*")
public class ExampleDataController extends Controller{

    @GetMapping("/api/initexampledata")
    public String run() {
        Address address1 = new Address(
                "Kraków",
                "Mazowiecka",
                "30-000",
                "60a",
                "5");

        Address address2 = new Address(
                "Rzeszów",
                "Mostowa",
                "35-000",
                "12",
                "8b");

        Person person1 = new Person(
                "Jan",
                "Kowalski",
                "56214785231"
        );

        Person person2 = new Person(
                "Katarzyna",
                "Kowalska",
                "58422561215"
        );

        Person person3 = new Person(
                "Tomasz",
                "Nowak",
                "95156321584"
        );

        Deposit deposit1 = new Deposit("45215846512542365874264215");
        Deposit deposit2 = new Deposit("12563201501563215015151521");
        Deposit deposit3 = new Deposit("85232145862125632154221521");
        Deposit deposit4 = new Deposit("48941654198192949645654167");

        Transfer transfer1 = new Transfer(400);
        Transfer transfer2 = new Transfer(600);
        Transfer transfer3 = new Transfer(800);
        Transfer transfer4 = new Transfer(300);
        Transfer transfer5 = new Transfer(-500);
        Transfer transfer6 = new Transfer(700);

        Credit credit1 = new Credit(1000, 5, 4.9, null);
        Credit credit2 = new Credit(25000, 48, 3, null);

        Worker worker = new Worker("root", "root");

        address1 = as.create(address1);
        address2 = as.create(address2);

        person1.setAddress(address1);
        person2.setAddress(address1);
        person3.setAddress(address2);

        person1 = ps.create(person1);
        person2 = ps.create(person2);
        person3 = ps.create(person3);

        deposit1 = ds.create(deposit1, person1);
        deposit2 = ds.create(deposit2, person2);
        deposit3 = ds.create(deposit3, person3);
        deposit4 = ds.create(deposit4, person3);

        ts.create(transfer1, deposit1);
        ts.create(transfer2, deposit2);
        ts.create(transfer3, deposit3);
        ts.create(transfer4, deposit1);
        ts.create(transfer5, deposit2);
        ts.create(transfer6, deposit3);

        cs.create(credit1, deposit1);
        cs.create(credit2, deposit4);

        ws.store(worker);

        return "Dane dodane";
    }
}
