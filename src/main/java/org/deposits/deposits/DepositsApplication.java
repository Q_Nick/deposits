package org.deposits.deposits;

import org.deposits.deposits.repositories.PersonRepository;
import org.deposits.deposits.entities.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DepositsApplication{
    public static void main(String[] args) {
        SpringApplication.run(DepositsApplication.class, args);
    }

    public void run(String... args){
    }
}
