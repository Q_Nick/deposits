package org.deposits.deposits.services;

import org.deposits.deposits.entities.Address;
import org.deposits.deposits.entities.Person;
import org.deposits.deposits.repositories.AddressRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AddressService {
    @Autowired
    private AddressRepository addressRepository;

    @Autowired
    private  PersonService personService;

    public Address create(Address address){
        return addressRepository.save(address);
    }

    public Address get(int id){
        return addressRepository.findSingleById(id);
    }

    public List<Address> get(){
        return addressRepository.findAll();
    }

    public Address updateCity(int id, String city){
        if(addressRepository.countById(id)==1){
            Address address=addressRepository.findSingleById(id);
            address.setCity(city);
            return addressRepository.save(address);
        }else{
            return null;
        }
    }

    public Address updateStreet(int id, String street){
        if(addressRepository.countById(id)==1){
            Address address=addressRepository.findSingleById(id);
            address.setStreet(street);
            return addressRepository.save(address);
        }else{
            return null;
        }
    }

    public Address updateAddressCode(int id, String addressCode){
        if(addressRepository.countById(id)==1){
            Address address=addressRepository.findSingleById(id);
            address.setAddressCode(addressCode);
            return addressRepository.save(address);
        }else{
            return null;
        }
    }

    public Address updateBuildingNumber(int id, String buildingNumber){
        if(addressRepository.countById(id)==1){
            Address address=addressRepository.findSingleById(id);
            address.setBuildingNumber(buildingNumber);
            return addressRepository.save(address);
        }else{
            return null;
        }
    }

    public Address updateFlatNumber(int id, String flatNumber){
        if(addressRepository.countById(id)==1){
            Address address=addressRepository.findSingleById(id);
            address.setFlatNumber(flatNumber);
            return addressRepository.save(address);
        }else{
            return null;
        }
    }

    public boolean remove(int id){
        if(addressRepository.countById(id)==1){

            List<Person>people=personService.get(addressRepository.findSingleById(id));
            for(Person person : people){
                personService.remove(person);
            }

            addressRepository.deleteById(id);
            return true;
        }
        else{
            return false;
        }
    }
}
