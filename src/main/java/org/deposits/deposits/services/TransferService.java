package org.deposits.deposits.services;

import org.deposits.deposits.entities.Transfer;
import org.deposits.deposits.entities.Deposit;
import org.deposits.deposits.repositories.TransferRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TransferService{
    @Autowired
    private TransferRepository transferRepository;

    @Autowired
    private DepositService ds;

    public List<Transfer> get(){
        return transferRepository.findAll();
    }

    public List<Transfer> get(Deposit deposit){
        return transferRepository.findByDeposit(deposit);
    }

    public Transfer get(int id){
        return transferRepository.findSingleById(id);
    }

    public Transfer create(Transfer transfer, Deposit deposit){
        transfer.setDeposit(deposit);
        ds.updateValue(deposit, transfer.getValue());
        return transferRepository.save(transfer);
    }

    public Transfer updateTitle(int id, String title){
        if(transferRepository.countById(id)==1){
            Transfer transfer=transferRepository.findSingleById(id);
            return transferRepository.save(transfer);
        }else{
            return null;
        }
    }
    public Transfer updateValue(int id, double value){
        if(transferRepository.countById(id)==1){
            Transfer transfer=transferRepository.findSingleById(id);
            transfer.setValue(value);
            return transferRepository.save(transfer);
        }else{
            return null;
        }
    }

    public Transfer updateDeposit(int id, Deposit deposit){
        if(transferRepository.countById(id)==1){
            Transfer transfer=transferRepository.findSingleById(id);
            transfer.setDeposit(deposit);
            return transferRepository.save(transfer);
        }else{
            return null;
        }
    }

    public boolean remove(int id){
        if(transferRepository.countById(id)==1){
            transferRepository.deleteById(id);
            return true;
        }else{
            return false;
        }
    }

    public double getValue(Deposit deposit){
        return transferRepository.getSumValueByDeposit(deposit);
    }
}
