package org.deposits.deposits.services;

import org.deposits.deposits.entities.Address;
import org.deposits.deposits.entities.Person;
import org.deposits.deposits.entities.Deposit;
import org.deposits.deposits.repositories.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PersonService{
    @Autowired
    private PersonRepository personRepository;

    @Autowired
    private DepositService depositService;

    public Person create(Person person){
        return personRepository.save(person);
    }

    public Person get(int id){
        Person person=this.personRepository.findSingleById(id);
        return person;
    }

    public List<Person> get(){
        return personRepository.findAll();
    }

    public List<Person> get(Address address){
        return personRepository.findByAddress(address);
    }

    public Person updateFirstName(int personId, String firstName){
        if(personRepository.countById(personId)==1){
            Person person=personRepository.findSingleById(personId);
            person.setFirstName(firstName);
            return personRepository.save(person);
        }
        else{
            return null;
        }
    }

    public Person updateLastName(int personId, String lastName){
        if(personRepository.countById(personId)==1){
            Person person=personRepository.findSingleById(personId);
            person.setLastName(lastName);
            return personRepository.save(person);
        }
        else{
            return null;
        }
    }

    public Person updatePesel(int personId, String pesel){
        if(personRepository.countById(personId)==1){
            Person person=personRepository.findSingleById(personId);
            person.setPesel(pesel);
            return personRepository.save(person);
        }
        else{
            return null;
        }
    }

    public Person updateAddress(int personId, Address address){
        if(personRepository.countById(personId)==1){
            Person person=personRepository.findSingleById(personId);
            person.setAddress(address);
            return personRepository.save(person);
        }
        else{
            return null;
        }
    }

    public boolean remove(int personId){
        if(personRepository.countById(personId)==1){

            List<Deposit> deposits=depositService.get(personRepository.findSingleById(personId));
            for(Deposit deposit :deposits){
                depositService.remove(deposit);
            }

            personRepository.deleteById(personId);
            return true;
        }
        else{
            return false;
        }
    }

    public boolean remove(Person person){
        if(personRepository.countById(person.getId())==1){
            List<Deposit> deposits=depositService.get(personRepository.findSingleById(person.getId()));
            for(Deposit deposit :deposits){
                depositService.remove(deposit);
            }

            personRepository.delete(person);
            return true;
        }
        else{
            return false;
        }
    }
}
