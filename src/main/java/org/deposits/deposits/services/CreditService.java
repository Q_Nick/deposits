package org.deposits.deposits.services;

import org.deposits.deposits.entities.Credit;
import org.deposits.deposits.entities.Deposit;
import org.deposits.deposits.repositories.CreditRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CreditService {
    @Autowired
    CreditRepository creditRepository;

    @Autowired DepositService ds;

    public List<Credit> get(){
        return creditRepository.findAll();
    }

    public Credit get(int id){
        return creditRepository.findSingleById(id);
    }

    public List<Credit> get(Deposit deposit){
        return creditRepository.findByDeposit(deposit);
    }

    public Credit create(Credit credit){
        Deposit deposit=credit.getDeposit();
        ds.updateValue(deposit, credit.getValue());
        return creditRepository.save(credit);
    }

    public Credit create(Credit credit, Deposit deposit){
        credit.setDeposit(deposit);
        ds.updateValue(deposit, credit.getValue());
        return creditRepository.save(credit);
    }

    public boolean paidInstallmentFromDeposit(Credit credit){

        if(credit.getDeposit().getValue()>=credit.getInstallmentValue()) {
            credit.setInstallmentsToPaid(credit.getInstallmentsToPaid() - 1);

            Deposit deposit = credit.getDeposit();

            ds.updateValue(deposit, credit.getInstallmentValue()*(-1));

            if (credit.getInstallmentsToPaid() <= 0) {
                creditRepository.deleteById(credit.getId());
            }

            return true;
        }else{
            return false;
        }
    }

    public boolean remove(Deposit deposit){
        List<Credit> credits=creditRepository.findByDeposit(deposit);

        System.out.println("------------->funkcja");

        if(credits.size()>0){
            System.out.println("------------->if");

            for(int i=0; i<credits.size(); i++){
                System.out.println("------------->pętla");

                creditRepository.delete(credits.get(i));
            }
            System.out.println("------------->za pętlą");

            return true;
        }else{
            return false;
        }
    }
}
