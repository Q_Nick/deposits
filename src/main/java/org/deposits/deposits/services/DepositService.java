package org.deposits.deposits.services;

import java.util.List;

import org.deposits.deposits.entities.Credit;
import org.deposits.deposits.entities.Deposit;
import org.deposits.deposits.entities.Person;
import org.deposits.deposits.entities.Transfer;
import org.deposits.deposits.repositories.DepositRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DepositService{
    @Autowired
    private DepositRepository depositRepository;

    @Autowired TransferService transferService;

    @Autowired CreditService creditService;

    public List<Deposit> get(){
        return depositRepository.findAll();
    }

    public List<Deposit> get(Person person){
        return depositRepository.findByPerson(person);
    }

    public Deposit get(int id){
        return depositRepository.findSingleById(id);
    }

    public Deposit create(Deposit deposit, Person person){
        deposit.setPerson(person);
        return depositRepository.save(deposit);
    }

    public Deposit updateDepositNumber(int id, String depositNumber){
        if(depositRepository.countById(id)==1){
            Deposit deposit=depositRepository.findSingleById(id);
            deposit.setDepositNumber(depositNumber);
            return depositRepository.save(deposit);
        }else{
            return null;
        }
    }

    public Deposit updatePerson(int id, Person person){
        if(depositRepository.countById(id)==1){
            Deposit deposit=depositRepository.findSingleById(id);
            deposit.setPerson(person);
            return depositRepository.save(deposit);
        }else{
            return null;
        }
    }

    public boolean remove(int id){
        if(depositRepository.countById(id)==1){

            List<Transfer> transfers=transferService.get(depositRepository.findSingleById(id));
            for(Transfer transfer : transfers){
                transferService.remove(transfer.getId());
            }

            creditService.remove(depositRepository.findSingleById(id));


            depositRepository.deleteById(id);
            return true;
        }else{
            return false;
        }
    }

    public boolean remove(Deposit deposit){
        if(depositRepository.countById(deposit.getId())==1){

            List<Transfer> transfers=transferService.get(depositRepository.findSingleById(deposit.getId()));
            for(Transfer transfer : transfers){
                transferService.remove(transfer.getId());
            }

            creditService.remove(deposit);

//            depositRepository.delete(deposit);
            return true;
        }else{
            return false;
        }
    }

    public void updateValue(Deposit deposit, double value){
        double newValue=deposit.getValue() + value;
        deposit.setValue(newValue);
        depositRepository.save(deposit);
    }
}
