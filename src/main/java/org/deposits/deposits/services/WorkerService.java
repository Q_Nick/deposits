package org.deposits.deposits.services;

import org.deposits.deposits.entities.Worker;
import org.deposits.deposits.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class WorkerService {

    @Autowired
    UserRepository userRepository;

    public Worker get(String username){
        return userRepository.findByUsername(username);
    }

    public Worker get(String username, String password){
        return userRepository.findByUsernameAndPassword(username, password);
    }

    public Worker store(Worker worker){
        return userRepository.save(worker);
    }

    public List<Worker> get(){
        return userRepository.findAll();
    }
}
