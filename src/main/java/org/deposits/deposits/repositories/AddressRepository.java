package org.deposits.deposits.repositories;

import org.deposits.deposits.entities.Address;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AddressRepository extends JpaRepository<Address, Integer> {
    Address findSingleById(int id);
    int countById(int id);
}
