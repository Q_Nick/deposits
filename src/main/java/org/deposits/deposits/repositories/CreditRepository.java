package org.deposits.deposits.repositories;

import org.deposits.deposits.entities.Credit;
import org.deposits.deposits.entities.Deposit;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CreditRepository extends JpaRepository<Credit, Integer> {
    public Credit findSingleById(int id);
    public List<Credit> findByDeposit(Deposit deposit);
    void deleteById(int id);
}
