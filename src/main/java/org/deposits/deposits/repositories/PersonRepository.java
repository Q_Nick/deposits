package org.deposits.deposits.repositories;

import org.deposits.deposits.entities.Address;
import org.deposits.deposits.entities.Person;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PersonRepository extends JpaRepository<Person, Integer> {
    Person findSingleById(int id);

    int countById(int id);

    List<Person> findByAddress(Address address);
    
    @Query("UPDATE Person p SET p.firstName=?1 WHERE p.id=?2")
    boolean setFirstNameWhereId(String firstName, int id);
}
