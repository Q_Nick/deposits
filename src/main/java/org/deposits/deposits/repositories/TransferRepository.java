/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.deposits.deposits.repositories;

import org.deposits.deposits.entities.Transfer;
import org.deposits.deposits.entities.Deposit;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 *
 * @author kamils
 */
@Repository
public interface TransferRepository extends JpaRepository<Transfer, Integer>{
    Transfer findSingleById(int id);

    int countById(int id);

    List<Transfer> findByDeposit(Deposit deposit);

    double getSumValueByDeposit(Deposit deposit);
}
