package org.deposits.deposits.repositories;

import org.deposits.deposits.entities.Worker;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<Worker, Integer> {
    public Worker findByUsername(String username);

    public Worker findByUsernameAndPassword(String username, String password);
}
