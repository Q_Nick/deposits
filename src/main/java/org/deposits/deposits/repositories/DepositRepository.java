package org.deposits.deposits.repositories;

import org.deposits.deposits.entities.Deposit;
import org.deposits.deposits.entities.Person;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DepositRepository extends JpaRepository<Deposit, Integer>{
    
    Deposit findSingleById(int id);
    
    Person getPersonById(int id);

    int countById(int id);

    List<Deposit> findByPerson(Person person);

}
