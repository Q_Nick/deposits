package org.deposits.deposits;

import org.deposits.deposits.entities.Worker;
import org.deposits.deposits.services.WorkerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired WorkerService workerService;

    private UserDetails userDetailsBuilder(Worker user){
        return User.withDefaultPasswordEncoder()
                .username(user.getUsername())
                .password(user.getPassword())
                .roles("USER")
                .build();
    }

    @Bean
    public UserDetailsService userDetailsService(){

        Collection<UserDetails> userDetailsCollection = new ArrayList<>();

        List<Worker> workers= (List<Worker>) workerService.get();

        for(int i=0; i<workers.size(); i++){
            userDetailsCollection.add(userDetailsBuilder(workers.get(i)));
        }

        return new InMemoryUserDetailsManager(userDetailsCollection);
    }

//    @Bean
//    public UserDetailsService userDetailsService(){
//        UserDetails userDetails=User.withDefaultPasswordEncoder()
//                .username("user")
//                .password("user")
//                .roles("USER")
//                .build();
//
//        return new InMemoryUserDetailsManager(userDetails);
//    }

//    @Override
//    protected void configure(HttpSecurity http){
//        try {
//            http.httpBasic().and().authorizeRequests()
//                    .anyRequest().authenticated()
//                    .and()
//                    .formLogin().permitAll()
//                    .and()
//                    .csrf().disable();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }


    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.cors();

        http.httpBasic().and().authorizeRequests()
                .antMatchers("/api/login", "/api/initexampledata").permitAll()
                .anyRequest().authenticated()
                .and()
                .csrf().disable();

    }
}